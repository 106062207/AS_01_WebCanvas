# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
* on the left menu:
    * brush
    * eraser
    * text: text in the textbox on the right menu and click on the position you want to draw the words.(**Note: at here, i implement the text feature by simulating stamp!**)
    * circle, triangle, triangle, line: drag on the canvas to draw graphics, respectively.
    * **stroke/fill**: click to choose between drawing stroke or filled graphics above.
* on the right menu:
    * color selector: click on right long and narrow rainbow-colored area to put the main color in the big pallete left; the current color using is showed at just below.
    * font size: select the size of texting on the canvas.
    * font family: select the style of texting on the canvas.
    * brush size: select the brush size of drawing on the canvas.(**Note: it also affect on the eraser size, the stroke size of graphics which choosen stroke.**)
    * textbox: text here for the text feature on the left menu.
    * undo, redo: presented by "UN", "RE", undo and redo the work on canvas, respectively.
    * reset: reset all works on canvas.
    * upload: upload image to draw on the canvas. (**Note: I use input with label wrapped in order to implement background image**)
    * download: download the canvas as image(**Note: Implemented by "a" attribute**).