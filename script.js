var canv;
var ctx;

var colorBlock;
var ctxBlock;

var colorStrip;
var ctxStrip;

var colorLabel;

var colorX, colorY;

var drag = false;

var fontSize_ = 4;
var fontFamily_ = "Arial";

var brushSize_ = 4;

var colorRGBA = 'rgba(255, 0, 0, 1)';

var statusArr = ['brush', 'eraser', 'circle', 'rectangle', 'triangle', 'line', 'text'];
var status;

var prevX;
var prevY;

var strokeFill;

var workArr = new Array();
var workStep = -1;

function init()
{
  canv = document.getElementById('canvas1');
  ctx = canv.getContext('2d');

  var div2 = document.getElementById('div2');
  canv.width = "1350";
  canv.height = "540";

  status = statusArr[0];
  strokeFill = 0;

  colorSelector();

  updateWork();

  for(var i = 0; i < statusArr.length; i++)
  {
    updateStatus(i);
  }

  var textbox = document.getElementById('textbox1');
  textbox.value = 'Text here';
  textbox.style.color = '#DDD';
  textbox.addEventListener('click',
  function()
  {
    textbox.style.color = '#000';
    if(textbox.value == 'Text here')
    {
      textbox.value = '';
    }
  })

  canv.addEventListener('click', 
  function(event)
  {
    if(status == statusArr[6])
    {
      var mousePos = getMousePos(canv, event);
      var text = document.getElementById('textbox1').value;
      ctx.textBaseline = 'top';
      ctx.fillText(text, mousePos.x, mousePos.y);

      updateWork();
    }
  })

  var getStrokefill = document.getElementById('strokeFill');

  getStrokefill.addEventListener('click',
  function()
  {
    strokeFill = !strokeFill;
    if(strokeFill)
    {
      getStrokefill.style.backgroundImage = "url('buttonImg/circleFilled.png')";
    }
    else
    {
      getStrokefill.style.backgroundImage = "url('buttonImg/circleStroke.png')";
    }

    console.log("strokeFill");
  })

  document.getElementById('reset').addEventListener('click',
  function()
  {
    ctx.clearRect(0, 0, canv.width, canv.height);
    workArr.length = 0;
    updateWork();

    console.log("reset");
  })

  document.getElementById('upload').addEventListener('change', function() {
    var img = new Image();
    img.onload = function()
    {
      ctx.drawImage(img, 0, 0, canv.width, canv.height);
    };
    img.onerror = function()
    {
      console.log("Failed to load the image");
    };
    img.src = URL.createObjectURL(this.files[0]);
  });

  document.getElementById('uploadLabel').addEventListener('mouseenter', function()
  {
    document.body.style.cursor = 'pointer';
  })

  document.getElementById('uploadLabel').addEventListener('mouseout', function()
  {
    document.body.style.cursor = 'auto';
  })

  document.getElementById('undo').addEventListener('click', undo);

  document.getElementById('redo').addEventListener('click', redo);

  canv.addEventListener("mousedown", mouseDown);

  canv.addEventListener("mouseup", mouseUp);

  canv.addEventListener('mouseenter', function()
  {
    switch(status)
    {
      case statusArr[0]:
        document.body.style.cursor = 'crosshair';
        break;
      case statusArr[1]:
        document.body.style.cursor = 'crosshair';
        break;
      case statusArr[2]:
      case statusArr[3]:
      case statusArr[4]:
      case statusArr[5]:
        document.body.style.cursor = 'all-scroll';
        break;
      case statusArr[6]:
        document.body.style.cursor = 'text';
        break;
      default:
        document.body.style.cursor = 'crosshair'
    }
    console.log(document.body.style.cursor);
  });

  canv.addEventListener("mouseout", 
  function()
  {
    canv.removeEventListener("mousemove", mouseMove);
    document.body.style.cursor = "auto";
  });

  var download = document.getElementById('btn-download');
  download.addEventListener('click', 
  function () 
  {
    var dataURL = canv.toDataURL('image/png');
    download.href = dataURL;
  });

  colorBlock.addEventListener("mousedown", function(event)
  {
    drag = true;
    updateColor(event);
  });

  colorBlock.addEventListener("mouseup", function()
  {
    drag = false;
  });

  colorBlock.addEventListener("mousemove", function(event)
  {
    if(drag)
    {
      updateColor(event);
    }
  });
}

function getMousePos(canv, event)
{
  var rect = canv.getBoundingClientRect();
  return{
    x: event.clientX - rect.left,
    y: event.clientY - rect.top
  };
}

async function mouseMove(event)
{
  var mousePos = getMousePos(canv, event);
  switch(status)
  {
    case statusArr[0]:
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
      break;
    case statusArr[1]:
      ctx.clearRect(mousePos.x, mousePos.y, brushSize_, brushSize_);
      break;
    case statusArr[2]:
      mouseMovesave(function()
      {
        ctx.beginPath();
        ctx.moveTo(prevX+(mousePos.x-prevX)/2 + Math.sqrt(Math.pow((mousePos.x-prevX), 2) + Math.pow((mousePos.y-prevY), 2))/2, prevY+(mousePos.y-prevY)/2);
        ctx.arc(prevX+(mousePos.x-prevX)/2, prevY+(mousePos.y-prevY)/2, 
                Math.sqrt(Math.pow((mousePos.x-prevX), 2) + Math.pow((mousePos.y-prevY), 2))/2,
                0, Math.PI * 2, true);
        ctx.closePath();
        if(strokeFill == 0)
        {
          console.log('draw');
          ctx.stroke();
        }
        else
        {
          ctx.fill();
        }
      });
      break;
    case statusArr[3]:
      mouseMovesave(function()
      {
        if(strokeFill == 0)
        {
          ctx.strokeRect(prevX, prevY, (mousePos.x-prevX), mousePos.y-prevY);
        }
        else
        {
          ctx.fillRect(prevX, prevY, (mousePos.x-prevX), mousePos.y-prevY);
        }
      });
      break;
    case statusArr[4]:
      mouseMovesave(function()
      {
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        ctx.lineTo(prevX+(mousePos.x-prevX)/2, prevY);
        ctx.lineTo(prevX, mousePos.y);
        ctx.closePath();
        if(strokeFill == 0)
          ctx.stroke();
        else
          ctx.fill();
      });
      break;
    case statusArr[5]:
      mouseMovesave(function()
      {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
      })
      break;
    case statusArr[6]:
      break;
    default:
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
  }
}

function mouseDown(event)
{
  var mousePos = getMousePos(canv, event);

  ctx.beginPath();
  ctx.moveTo(mousePos.x, mousePos.y);
  prevX = mousePos.x;
  prevY = mousePos.y;

  canv.addEventListener("mousemove", mouseMove);
}  

function mouseUp(event)
{
  canv.removeEventListener("mousemove", mouseMove)

  var mousePos = getMousePos(canv, event);
  switch(status)
  {
    case statusArr[0]:
      break;
    case statusArr[1]:
      break;
    case statusArr[2]:
      ctx.moveTo(prevX+(mousePos.x-prevX)/2 + Math.sqrt(Math.pow((mousePos.x-prevX), 2) + Math.pow((mousePos.y-prevY), 2))/2, prevY+(mousePos.y-prevY)/2);
      ctx.arc(prevX+(mousePos.x-prevX)/2, prevY+(mousePos.y-prevY)/2, 
              Math.sqrt(Math.pow((mousePos.x-prevX), 2) + Math.pow((mousePos.y-prevY), 2))/2,
              0, Math.PI * 2, true);
      ctx.closePath(); 
      if(strokeFill == 0)
        ctx.stroke();
      else
        ctx.fill();
      break;
    case statusArr[3]:
      if(strokeFill == 0)
        ctx.strokeRect(prevX, prevY, (mousePos.x-prevX), mousePos.y-prevY);
      else
        ctx.fillRect(prevX, prevY, (mousePos.x-prevX), mousePos.y-prevY);
      break;
    case statusArr[4]:
      ctx.moveTo(mousePos.x, mousePos.y);
      ctx.lineTo(prevX+(mousePos.x-prevX)/2, prevY);
      ctx.lineTo(prevX, mousePos.y);
      ctx.closePath();
      if(strokeFill == 0)
        ctx.stroke();
      else
        ctx.fill();
      break;
    case statusArr[5]:
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
      break;
    case status[6]:
      break;
    default:
  }
  if(status !== statusArr[6])
    updateWork();
}

function updateStatus(i)
{
  document.getElementById(statusArr[i]).addEventListener('click', 
  function()
  {
    status = statusArr[i];

    console.log(statusArr[i]);
  })
}

function updateWork()
{
  workStep++;
  if(workStep < workArr.length)
  {
    workArr.length = workStep;
  }

  workArr.push(canv.toDataURL('image/png'));

  console.log("updateWork");
}

function undo()
{
  if(workStep > 0)
  {
    workStep--;
    var canvImg = new Image();
    canvImg.src = workArr[workStep];
    canvImg.onload = function()
    {
      ctx.clearRect(0, 0, canv.width, canv.height);
      ctx.drawImage(canvImg, 0, 0);
    }
  }

  console.log("undo");
}

function redo()
{
  if(workStep < workArr.length-1)
  {
    workStep++;
    var canvImg = new Image();
    canvImg.src = workArr[workStep];
    canvImg.onload = function()
    {
      ctx.drawImage(canvImg, 0, 0);
    }
  }

  console.log("redo");
}

function colorSelector()
{
  colorBlock = document.getElementById('canvas2');
  ctxBlock = colorBlock.getContext('2d');

  colorStrip = document.getElementById('canvas3');
  ctxStrip = colorStrip.getContext('2d');
  
  colorBlock.width = "150";
  colorBlock.height = "150";
  colorStrip.width = "30";
  colorStrip.height = "150";

  colorLabel = document.getElementById('colorLabel');

  fillGradient();
  ctxBlock.fillRect(0, 0, colorBlock.width, colorBlock.height);

  var grd1 = ctxStrip.createLinearGradient(0, 0, 0, colorStrip.height);
  grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
  grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
  grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
  grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
  grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
  grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
  grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
  ctxStrip.fillStyle = grd1;
  ctxStrip.fillRect(0, 0, colorStrip.width, colorStrip.height);
}

function fillGradient() {
  ctxBlock.fillStyle = colorRGBA;
  ctxBlock.fillRect(0, 0, colorBlock.width, colorBlock.height);

  var grdWhite = ctxBlock.createLinearGradient(0, 0, colorBlock.width, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctxBlock.fillStyle = grdWhite;
  ctxBlock.fillRect(0, 0, colorBlock.width, colorBlock.height);

  var grdBlack = ctxBlock.createLinearGradient(0, 0, 0, colorBlock.height);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctxBlock.fillStyle = grdBlack;
  ctxBlock.fillRect(0, 0, colorBlock.width, colorBlock.height);

  colorStrip.addEventListener("click", function(event)
  {
    var mousePos = getMousePos(colorStrip, event);
    var imageData = ctxStrip.getImageData(mousePos.x, mousePos.y, 1, 1).data;
    colorRGBA = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    fillGradient();
  });
}

function updateColor(event)
{
  var mousePos = getMousePos(colorBlock, event);
  var imageData = ctxBlock.getImageData(mousePos.x, mousePos.y, 1, 1).data;
  colorRGBA = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  colorLabel.style.backgroundColor = colorRGBA;
  ctx.fillStyle = colorRGBA;
  ctx.strokeStyle = colorRGBA;
}

function fontSize(n)
{
  fontSize_ = n.value + 'px';
  ctx.font = ctx.font.replace(/\d+px/, fontSize_);
  console.log("fontSize: "+fontSize_);
}

function fontFamily(n)
{
  fontFamily_ = n.value;
  ctx.font = fontSize_ + ' ' + fontFamily_;
}

function brushSize(n)
{
  brushSize_ = n.value;
  ctx.lineWidth = n.value;
}

function mouseMovesave(callback) {
  var canvImg = new Image();
  
  canvImg.onload = function()
  {
    ctx.clearRect(0, 0, canv.width, canv.height);
    ctx.drawImage(canvImg, 0, 0);

    callback();
  }
  canvImg.src = workArr[workStep];
}